<?php

namespace Controllers;

/**
 * Class Email
 * @package Controllers
 */
class Email extends Base {

    /**
     * Check email from post-data
     */
    public function check_post(){
        $data = json_decode(file_get_contents('php://input'));
        $result = self::validate($data->email);
        $this->json($result);
    }

    /**
     * Email validation and empty-check
     * @param $email
     * @return array
     */
    public static function validate ($email){
        if($email == "")
            return [
                'validation' => false,
                'error' => 'EMAIL_REQUIRED'
            ];
        else if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            return [
                'validation' => false,
                'error' => 'EMAIL_BAD'
            ];
        else
            return [
                'validation' => true
            ];
    }
}

?>