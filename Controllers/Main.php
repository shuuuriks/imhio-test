<?php

namespace Controllers;

/**
 * Main page controller
 * Class Main
 * @package Controllers
 */
class Main extends Base {
    /**
     * index page
     */
    public function index(){
        $this->test = [
           'var1' => 1,
           'var2' => 2
        ];
        $this->view();
    }

    public function not_found(){
        $this->view();
    }
}

?>