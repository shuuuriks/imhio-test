<?php
    namespace Controllers;

    /**
     * Base abstract controller
     * Class Base
     * @package Controllers
     */
    abstract class Base {
        /**
         * Page title
         * @var string
         */
        public $title = "IMHIO TEST";

        /**
         * Page template path
         * @var string
         */
        private $_template = "/Views/main/index.php";
        /**
         * Implemented scripts
         * @var array
         */
        private $_js = [];

        /**
         * Return json response
         * @param $data
         */
        public function json($data){
            echo json_encode($data);
            exit;
        }

        /**
         * Include template
         */
        public function view(){
            include $_SERVER['DOCUMENT_ROOT'] . '/Views/header.html';
            include $_SERVER['DOCUMENT_ROOT'] . $this->_template;
            foreach ($this->_js as $src) {
                echo "<script type='application/javascript' src='{$src}'></script>";
            }
            include $_SERVER['DOCUMENT_ROOT'] . '/Views/footer.html';
        }

        /**
         * Set template
         * @param $name
         */
        public function setTemplate($name){
            $this->_template = $name;
        }

        /**
         * Add path tot implemented scripts
         * @param $name
         */
        public function addJS($name){
            $this->_js[] = $name;
        }
    }
?>