;(function () {

    var $form = document.body.querySelector('.js-form');
    var $email = $form.querySelector('.js-email');
    var $error = $form.querySelector('.js-error');
    var $success = $form.querySelector('.js-success');

    /**
     * Check response with validation result
     */
    var response = function () {
        if(this.readyState !== 4)
            return;
        var result = JSON.parse(this.responseText);

        if(!result.validation){
            $email.className = "email error js-email";
            switch (result.error) {
                case 'EMAIL_REQUIRED':
                    $error.innerHTML = 'введите email';
                    break;
                case 'EMAIL_BAD':
                    $error.innerHTML = 'некорректный email';
                    break;
                default:
                    $error.innerHTML = 'неизвестная ошибка';
                    break;
            }
        } else {
            $success.innerHTML = 'email прошел проверку';
        }
    };

    /**
     * Send request to email validation
     * @param e - submit event
     */
    var request = function (e) {
        e.preventDefault();

        $email.className = "email js-email";
        $error.innerHTML = '';
        $success.innerHTML = '';

        // console.log($email.value);
        // console.log($form.method);

        var xhr = new XMLHttpRequest();

        var json = JSON.stringify({
            email: $email.value
        });

        xhr.open($form.method, $form.action, true);

        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.onreadystatechange = response;

        xhr.send(json);
    };

    $form.onsubmit = request;
})();