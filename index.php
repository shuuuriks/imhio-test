<?php

include($_SERVER['DOCUMENT_ROOT'].'/config.php');

error_reporting(E_ALL);
ini_set('display_errors', $config['debug'] ? 1 : 0);

spl_autoload_register(function ($class) {
    $path = str_replace('\\','/', $class);
    include $_SERVER['DOCUMENT_ROOT'] . '/' . $path . '.php';
});

include($_SERVER['DOCUMENT_ROOT'].'/routes.php');

?>