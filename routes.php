<?php

$method = $_SERVER['REQUEST_METHOD'];
$request = array_key_exists('path', $_GET) ? $_GET['path'] : null;

$matches = array();

preg_match("/([^\/]*)\/?([^\/]*)?\/?/", $request,$matches);

if($matches[1] == ""){
    $matches[1] = "main";
}

if($matches[2] == ""){
    $matches[2] = "index";
}

try{
    $C = "\Controllers\\".ucfirst($matches[1]);
    if(!class_exists($C)){
        $matches[1] = "main";
        $matches[2] = "not_found";

        $C = "\Controllers\\".ucfirst($matches[1]);
    }

    $c = new $C;

    $m = $matches[2];

    if($method !== 'GET'){
        $m .= "_".strtolower($method);
    }

    $template = sprintf("/Views/%s/%s.php",$matches[1],$matches[2]);
    $script = sprintf("/js/%s/%s.js",$matches[1],$matches[2]);

//    echo $template;

    $c->addJS($script);
    $c->setTemplate($template);

    $c->$m();
} catch (Exception $e) {
    echo $e;
}


?>